import kotlin.math.PI
import kotlin.math.pow


abstract class  Form(){
    abstract fun aire():Double
}

class Circle(val rayon:Double):Form(){
    override fun aire():Double{
        return rayon.pow(2)*PI
    }
//    fun getRayon():Double{
//        return rayon
//    }
}

class Rectangle(val width:Double, val height:Double):Form(){
    override fun aire():Double{
        return width*height
    }
}

class Triangle(val base:Double, val height:Double):Form(){
    override fun aire():Double{
        return (base * height)/2
    }
}

fun main (){
    var circleArray :List<Circle> = listOf()
    for (i in 1..5){
    val rayonCircle: Double = (0..100).random().toDouble()
        circleArray += Circle(rayonCircle)
    }
    circleArray.mapIndexed(){ index, circle -> println("Voici l'air du cercle numéro : ${index + 1} d'un rayon de ${circle.rayon} et qui a une aire de ${circle.aire()}")}

    val circle1 = Circle(5.0)
    val rectangle1 = Rectangle(5.0, 6.2)
    val triangle1 = Triangle(5.6, 10.0)
    var formsList: List<Form> = listOf(circle1, rectangle1,triangle1)
    formsList.map{form -> println(form.aire())}
}