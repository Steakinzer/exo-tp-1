abstract class Animal(val nom: String, val type: String) {
    open fun faireDuBruit(){
        println("Bruit de l'animal")
    }
}

interface Nageur{
    fun nager(){
        println("L'animal nage")
    }
}

interface Voleur {
    fun voler(){
        println("L'animal est un voleur")
    }
}
class Chien(nom:String, type:String) : Animal(nom, type){
    override fun faireDuBruit() {
        println("Wouaf wouaf")
    }
}

class Chat(nom:String, type:String) : Animal(nom, type){
    override fun faireDuBruit() {
        println("Miaou")
    }
}

class Aigle(nom:String, type:String) : Animal(nom, type), Voleur{
    override fun faireDuBruit() {
        println("Piou Piou")
    }
}
class Dauphin(nom:String, type:String) : Animal(nom, type), Nageur{
    override fun faireDuBruit() {
        println("Blup Blup")
    }
}

fun main() {
    var chien1 = Chien("patrick","cauet")
    println(chien1.faireDuBruit())

    var dauphin1 = Dauphin("Damien", "Rieu")
    println(dauphin1.nager())

    var aigle1 = Aigle("Julien", "Patou")
    println(aigle1.voler())
}