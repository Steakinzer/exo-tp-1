class CompteBancaire(private var solde:Double){
    fun deposer(montant:Double){
        solde += montant
    }
    fun retirer(montant:Double){
        if (solde > montant) {
            solde -= montant
        }else{
            println("Vous n'avez pas le montant suffisant sur votre compte en banque")
        }
    }
    fun consulterSolde(){
        println(solde)
    }
}

fun main(){
    var compte1 = CompteBancaire(5000.0)
    compte1.deposer(200.0)
    compte1.consulterSolde()
    compte1.retirer(598.5)
    compte1.consulterSolde()
    compte1.retirer(6000.0)
}