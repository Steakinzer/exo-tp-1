abstract class Forme(){
    open fun afficher(){
        println("Forme générique")
    }
}

class Trianglee(var width:Double, var height:Double):Forme(){
    override fun afficher() {
        println("Triangle de base: $width et de hauteur : $height")
    }
}


class Carree(var width:Double, var height:Double):Forme(){
    override fun afficher() {
        println("Carré de largeur : $width et de hauteur : $height")
    }
}


class Circlee(var rayon:Double):Forme(){
    override fun afficher() {
        println("Cercle de rayon : $rayon")
    }
}

fun main(){
    var circle1:Forme = Circlee(5.3)
    var carre1 :Forme = Carree(5.0,6.5)
    var triangle1 :Forme = Trianglee(5.2,9.1)
    var tableauForms: List<Forme> = listOf(circle1,carre1,triangle1)
    tableauForms.map{ form -> form.afficher()}
}