fun <T> afficheList(list: List<T>){
    println(list)
}

class Boite<T>(var contenu :T){
    fun afficherContenu():T{
        return contenu
    }
}
fun main(){
    var liste1 = listOf("blabla","blupblup","pokezeopfkz",52,true,50.656456)
    afficheList(liste1)

    var boite:Boite<String> = Boite("Une peluche")
    var boite2:Boite<Int> = Boite(50)
    var boite3:Boite<Boolean> = Boite(true)
    var liste2 = listOf(boite.afficherContenu(),boite2.afficherContenu(),boite3.afficherContenu())
    afficheList(liste2)
}