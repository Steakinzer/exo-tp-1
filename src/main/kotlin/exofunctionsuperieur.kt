import java.util.*

fun main(){
    var eatingmenu:MutableList<String> = mutableListOf("\uD83C\uDF5F", "\uD83C\uDF54", "\uD83C\uDF5F", "\uD83C\uDF54", "\uD83C\uDF5F", "\uD83C\uDF54", "\uD83C\uDF5F", "\uD83C\uDF54", "\uD83C\uDF5F")
    println(eatingmenu)
    var onlyburger = eatingmenu.toMutableList()
    Collections.replaceAll(onlyburger, "\uD83C\uDF5F","\uD83C\uDF54")
    println(onlyburger)
    var filterfries = eatingmenu.filter { it == "\uD83C\uDF5F" }
    println(filterfries)
    eatingmenu.forEachIndexed() { index, item -> if (item == "\uD83C\uDF5F") println("Voici l'index des frites : $index")}
    eatingmenu.forEachIndexed() {index,item -> if (index >=1) eatingmenu[index] = "\uD83C\uDF54"}
    println(eatingmenu)
    println(eatingmenu.contains("\uD83C\uDF54"))
    println(eatingmenu.all{it == "\uD83C\uDF54"})

}