class User (var name:String, val age:Int, val email:String){

}

fun main(){
    val user1 = User("Clement",28, "clementjodin@gmail.com")
    val user2 = User("Damien",24, "damien@gmail.com")
    val user3 = User("Patrice",22, "patrice@gmail.com")
    val user4 = User("Jean",29, "jean@gmail.com")
    val user5 = User("Daniel",15, "daniel@gmail.com")
    val user6 = User("DanPapy",85, "papy@gmail.com")

    val userList:List<User> = listOf(user1,user2,user3,user4,user5,user6)
    fun printUsers(userList:List<User>){
        for (user in userList){
            println("Nom : ${user.name}, age : ${user.age} email: ${user.email}")
        }
    }

    fun changeNames(userList:List<User>,newName:String):List<User>{
        for (user in userList){
            user.name = newName
        }
        return userList
    }

    val overTwentyFive:List<User> = userList.filter {it.age > 25}
    printUsers(overTwentyFive)
    changeNames(overTwentyFive, "Bonjour")
    printUsers(overTwentyFive)

    val emailsSet:MutableSet<String> = mutableSetOf("test1@gmail.com","test2@gmail.com", "test3@gmail.com")
    val usersMap:MutableMap<String,String> = mutableMapOf(
        "test1" to "test1@gmail.com",
        "test2" to "test2@gmail.com",
        "test3" to "test3@gmail.com"
    )

    val groupedUsers = userList.groupBy ({
        if (it.age < 18 ) "Mineur"
        else if (it.age > 18 && it.age < 50) "Adulte"
        else "Senior"
        },
        {it.email})
    println(groupedUsers)
}
