import java.lang.Exception
import java.util.*

fun main(){
    fun dividerCalculator():Float{

    val scan = Scanner(System.`in`)
    println("Choisissez un premier nombre")
        while(!scan.hasNextInt()){
            println("Veuillez entrer un nombre valide")
            scan.nextLine()
        }
        var firstNumber:Int = scan.nextInt().toInt()
    println("Choisissez un deuxième nombre")
        do{
            println("Veuillez entrer un nombre valide")
            scan.nextLine()
        }while(!scan.hasNextInt())
    var secondNumber:Int = scan.nextInt().toInt()
    var result :Float = 0.0f

       if (secondNumber == 0){
           throw Exception("Vous ne pouvez pas diviser un nombre par 0")
       }else {
           result = firstNumber.toFloat() / secondNumber.toFloat()
       }
        return result
    }
    try{
        println(dividerCalculator())
    }catch (e: Exception){
        println(e.message)
    }finally {
        println("Programme terminé")
    }


}