fun main(){
    fun String.toCamel():String{
        if (this.isEmpty()){
            return this
        }
        var splittedString = this.toList()
        return (splittedString[0].uppercaseChar().toString() + splittedString.subList(1,splittedString.size).joinToString(""))

    }
    fun String.toCamell():String{
        var tableOfWords:MutableList<String> = this.split(" ").toMutableList()
        var tempTable:MutableList<String> = mutableListOf()
        var index = 0
        for (word in tableOfWords){
            if (index == 0){
                tempTable.add(word)
                index++
            }else{

            tempTable.add(word.toCamel())
            }
        }

        return tempTable.joinToString("")
    }
    fun String.countWords():Int{
        return this.length
    }
    fun List<String>.filteredBy(filter:(String) -> Boolean):List<String>{
        return this.filter(filter)
    }

    fun List<*>.sumNumber():Int{
        var sum:Int = 0
        for (element in this){
            if (element is Number){
                sum += element.toInt()
            }
        }
        return sum
    }

    var maphrase:String = "mon fichier a enregistrer"
    var maliste:List<Any> = listOf("Oui", "Non", 50, 25, "Aglagla", 2.25)
    println(maphrase.toCamel())
    println(maphrase.toCamell())
    println(maphrase.countWords())
//    println(maliste.filteredBy{it == "Oui"})
    println(maliste.sumNumber())

}