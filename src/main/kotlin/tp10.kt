class Banque(val name:String){
    class Compte(var montant: Double){

        fun depot(depot:Double){
            montant += depot
        }
        fun retrait(retrait:Double){
            if (montant < retrait){
                println("Vous n'avez pas assez d'argent")
            }else{
                montant -= retrait
            }
        }
        inner class Transaction(val montant:Double){

        }
    }
}

fun main(){
    val banque1 :Banque = Banque("Crédit Mutuel")
    val compte1 = Banque.Compte(5652.52)
    compte1.depot(565.65)
    compte1.retrait(50000.0)
    compte1.retrait(500.0)
    val transaction1 = compte1.Transaction(500.0)
}