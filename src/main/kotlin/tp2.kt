class Personn(val name: String, val age: Int, val post: String) {
    constructor(name: String) : this(name, 0, "Non défini")
}
fun main(){
    val personn1 = Personn("John Doe", 30, "Développeur")

    println("Personne 1 Nom : ${personn1.name}, Age : ${personn1.age}, Poste : ${personn1.post}")

    val personn2 = Personn("Damien")
    println("Personne 2 : Nom : ${personn2.name} , Age : ${personn2.age} , Poste : ${personn2.post}")
}
