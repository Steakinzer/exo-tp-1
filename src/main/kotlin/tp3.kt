import java.util.*

class Employee(val name:String, val age:Int, val poste:String, var salaire:Float){
    fun afficherInfo(){
        println("Nom: ${name}, age: ${age}, poste: ${poste}, salaire: ${salaire}")
    }
    fun augmenterSalaire():Float{
        salaire = when(poste){
            "Manager" -> salaire* 1.10f
            "Développeur" -> salaire*1.15f
            "Comptable" ->  salaire*1.12f
            else -> salaire
        }
        return salaire
    }
}

fun main(){
    Locale.setDefault(Locale.US)
    val emp1 = Employee("Damien", 29, "Manager", 2780.58f)
    val emp2 = Employee("Patrice", 42, "Développeur", 4025.52f)
    val emp3 = Employee("Loic", 42, "Réalisateur", 2869.31f)

    emp1.afficherInfo()
    emp1.augmenterSalaire()
    emp1.afficherInfo()

    emp2.afficherInfo()
    emp2.augmenterSalaire()
    emp2.afficherInfo()

    emp3.afficherInfo()
    emp3.augmenterSalaire()
    emp3.afficherInfo()
    val emp4 = askEmployee()
    println("Employé 4 : ${emp4.name} , âge : ${emp4.age} , poste: ${emp4.poste} , salaire: ${emp4.salaire} €")
    println("Voulez vous une augmentation de salaire ? oui/non")
    val reader = Scanner(System.`in`)
    if (reader.next().lowercase() == "oui"){
        emp4.augmenterSalaire()
    }
    else{
        println("Vous avez choisi de rester pauvre!")
    }
}

fun askEmployee():Employee{
    val reader = Scanner(System.`in`)
    println("Entrez votre nom")
    var name:String = reader.next()
    var age:Int = 0
    var salaire:Float = 0.0f

    println("Veuillez entrer votre âge")
    while(!reader.hasNextInt()){
        println("Rentrez un âge valide")
        reader.next()
    }
    age = reader.nextInt()

    println("Entrez le nom de votre poste (exemple: Développeur)")
    var poste:String = reader.next()

    println("Veuillez entrer votre salaire")
    while(!reader.hasNextFloat()){
        println("Rentrez un salaire valide (avec les centimes)")
        reader.next()
    }
    salaire = reader.nextFloat()
    val emp4 = Employee(name, age, poste, salaire)
    return emp4
}