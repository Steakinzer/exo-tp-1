import java.util.concurrent.CountDownLatch

class Coureur(var coureurName: String, private val latch: CountDownLatch) : Thread() {
    var distanceCourse: Int = 200

    override fun run() {
        println("Le coureur $coureurName court")
        while (distanceCourse > 0) {
            println("Le coureur $coureurName a avancé")
            distanceCourse -= 1
        }
        println("Le coureur $coureurName est arrivé sur la ligne d'arrivée")
        latch.countDown()
    }
}

fun main() {
    val latch = CountDownLatch(1) // Attendre qu'un seul coureur termine
    val coureur1: Coureur = Coureur("Damien", latch)
    val coureur2: Coureur = Coureur("Polo", latch)
    val listeDeCoureur: List<Coureur> = listOf(coureur1, coureur2)

    fun course(listCoureur: List<Coureur>): String {
        for (coureur in listCoureur) {
            coureur.start()
        }
        latch.await() // Attendre que l'un des coureurs termine
        return "Vainqueur détecté"
    }

    println(course(listeDeCoureur))
}
