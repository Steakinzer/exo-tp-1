data class Personne(val name:String, val age:Int){

}
fun main(){
    val personne1: Personne = Personne("David", 29)
    val personne2 : Personne = Personne("Clement", 28)
    val personne3 :Personne = Personne("Clement",28)
    val personne4: Personne = personne1.copy()

    println(personne1 == personne2)
    println(personne1.hashCode())
    println(personne2.hashCode())

    println("Comparaison de deux personnes avec le même prénom et le meme âge")
    println(personne2 == personne3)
    println(personne2.hashCode())
    println(personne3.hashCode())
    println(personne1.toString())
    println(personne2.toString())
    println(personne3.toString())
    println("utilisation de la méthode equal : résultat entre personne de même nom et même age : ${personne2.equals(personne3)}")
    println("tostring de la copie de personne 1 qui est David 29 ans : ${personne4.toString()}")
    println("comparaison de la copy de David et de David, résultat : ${personne1 == personne4}")

}