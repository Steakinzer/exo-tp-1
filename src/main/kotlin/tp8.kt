abstract class Animall(){
    open fun faireDuBruit(){
        println("Je fait parti des animaux")
    }
}

class Chienn():Animall(){
    override fun faireDuBruit(){
    println("Je suis un chien")
    }
}

fun main(){
    var chien1: Animall = Chienn()
    chien1.faireDuBruit()
    var animal1 = chien1 as Animall
    animal1.faireDuBruit()

}