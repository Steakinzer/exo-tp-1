import java.awt.Point

class PointInteret(val nom:String, val localisation:String){


    val PI:Double = 3.236262565
    companion object{
        val PII:Double = 654654.564654
        fun create(nom:String, localisation:String):PointInteret{
            return PointInteret(nom, localisation)
        }

    }
}

fun main(){
    PointInteret.create("Haguenau","pokazedpozkef")
    var pts1 = PointInteret.create("Haguenau", "pokfezpokfoz")
    println(pts1.nom)
    println(pts1.localisation)
    println(PointInteret.PII)
    println(pts1.PI)
}