fun main() {
val treatFunction = trickOrTreat(true)
    val trickFunction = trickOrTreat(false)
    treatFunction()
    trickFunction()
}
fun trickOrTreat(isTrick: Boolean):() -> Unit{
    if (isTrick){
        return trick
    }else{
        return treat
    }
}
val trick ={ println("No treats!") }
val treat:() -> Unit = { println("Have a treat!")}