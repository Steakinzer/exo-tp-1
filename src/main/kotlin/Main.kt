import java.text.DecimalFormat
import java.util.*

fun main(args: Array<String>) {
    Locale.setDefault(Locale.US)
    var promotion :Boolean
    println("Entrez le nom du produit")
    var productName :String = readlnOrNull() ?: ""
    println("Entrez le prix du produit")
    var price :Float = readlnOrNull()?.toFloatOrNull() ?: 0.0f
    println("Entrez la quantité")
    var quantity :Int = readlnOrNull()?.toIntOrNull() ?: 0
    println("Entrez le code du produit")
    var codeProduct :Int = readlnOrNull()?.toIntOrNull() ?: 0
    println("Est-ce que le produit est en promotion?")
    var inputdecision = readlnOrNull()?.lowercase()
    if (inputdecision == "oui" || inputdecision == "yes") {
        promotion = true
    }
    else {
        promotion = false
    }
    println("nom : $productName")
    println("prix $price")
    println("quantité : $quantity")
    println("code produit : $codeProduct")
    if (promotion == true) {
        println("Le produit est en promotion")
    }
    else {
        println("Le produit n'est pas en promotion")
    }
    println("Veuillez saisir votre quantité d'achat")
    var requiredQuantity :Int = readlnOrNull()?.toIntOrNull()?:0
    var totalPrice :Float = price*requiredQuantity.toFloat()
    val df = DecimalFormat("#.##")
    println(requiredQuantity.toFloat())
    println(price)
    if (quantity < requiredQuantity){
        println("Le produit n'est plus en stock")

    }
    else {
        if (promotion) {
            println("Commande effectuée, une promotion de 10 % a été appliquée. prix total : ${df.format(totalPrice/100*90)}")
        }else{
        println("Commande effectuée, prix total : ${df.format(totalPrice)} €")
        }
    }
}